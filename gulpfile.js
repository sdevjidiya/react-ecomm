//** variable defination */
require("dotenv").config();
var gulp = require("gulp");
var concat = require("gulp-concat");
var cleanCSS = require("gulp-clean-css");
var uglify = require("gulp-uglify");
var order = require("gulp-order");
var urlAdjuster = require("gulp-css-url-adjuster");

let config = {
  cssPath: "public/assets/css",
  jsPath: "public/assets/js",
  distPath: "public/assets/dist",
  imgPath: `../../images`,
  fontPath: `../../fonts`,
  url: process.env.REACT_APP_URL,
};

//** Tasks */
gulp.task("pack-css", function () {
  return gulp
    .src([`${config.cssPath}/*.css`])
    .pipe(
      order([`${config.cssPath}/all.css`, `${config.cssPath}/rt-icons.css`], {
        base: "./",
      })
    )
    .pipe(
      urlAdjuster({
        replace: ["../images", `${config.imgPath}`],
      })
    )
    .pipe(
      urlAdjuster({
        replace: ["../fonts", `${config.fontPath}`],
      })
    )
    .pipe(concat("bundle.min.css"))
    .pipe(cleanCSS())
    .pipe(gulp.dest(`${config.distPath}/css`));
});

gulp.task("pack-js", function () {
  return gulp
    .src([`${config.jsPath}/*.js`, `${config.jsPath}/vendors/*.js`])
    .pipe(
      order(
        [
          `${config.jsPath}/vendors/jquery-3.6.0.min.js`,
          `${config.jsPath}/vendors/popper.min.js`,
          `${config.jsPath}/vendors/bootstrap.min.js`,
          `${config.jsPath}/vendors/wow.min.js`,
          `${config.jsPath}/vendors/swiper-bundle.min.js`,
          `${config.jsPath}/vendors/jquery.nstSlider.min.js`,
          `${config.jsPath}/vendors/jquery.nice-select.js`,
          `${config.jsPath}/vendors/zoom.js`,
          `${config.jsPath}/vendors/metisMenu.min.js`,
          `${config.jsPath}/vendors/rtsmenu.js`,
          `${config.jsPath}/vendors/isotope.pkgd.min.js`,
          `${config.jsPath}/vendors/jquery.magnific-popup.min.js`,
        ],
        { base: "./" }
      )
    )
    .pipe(concat("bundle.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest(`${config.distPath}/js`));
});

gulp.task("run", gulp.series("pack-css", "pack-js"));

// ** watcher */
gulp.task("watch", function () {
  gulp.watch(`${config.cssPath}/*.css`, gulp.series("pack-css"));
  gulp.watch(`${config.jsPath}/*.js`, gulp.series("pack-js"));
});

// ** default */
gulp.task("default", gulp.parallel("watch", "run"));
