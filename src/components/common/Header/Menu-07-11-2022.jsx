import React from 'react'
import { Link } from 'react-router-dom';

export default function Menu(props) {
  return (
    <React.Fragment>
      <li>
        <Link className="menu-item active1" to={`/`}>
          Home
        </Link>
      </li>
      <li className="has-dropdown">
        <Link className="menu-item" to="#">
          Categories <i className="rt-plus" />
        </Link>
        <ul className="dropdown-ul mega-dropdown">
          <li className="mega-dropdown-li">
            <ul className="mega-dropdown-ul">
              {
                Object.values(Object.assign({}, props.categories[0])).map((item, index) => {
                  return (
                    <li className="dropdown-li" key={index}>
                      <Link className="dropdown-link2" to={`/categories/${item.category_slug}`}>
                        {item.category_name}
                      </Link>
                      {Object.values(Object.assign({}, props.categories[item.uid])).map((sitem, sindex) => {
                        return (
                          <ul>
                            <li className="dropdown-li" style={{ marginLeft: '15px' }}>
                              <Link className="dropdown-link2" to={`/categories/${sitem.category_slug}`}>
                                {sitem.category_name}
                              </Link>
                              {Object.values(Object.assign({}, props.categories[sitem.uid])).map((titem, sindex) => {
                                return (
                                  <ul>
                                    <li className="dropdown-li" style={{ marginLeft: '15px' }}>
                                      <Link className="dropdown-link2" to={`/categories/${titem.category_slug}`}>
                                        {titem.category_name}
                                      </Link>
                                    </li>
                                  </ul>
                                )
                              })}
                            </li>
                          </ul>
                        )
                      })}
                    </li>
                  )
                })
              }
            </ul>
            {/* <ul className="mega-dropdown-ul">
              {
                Object.values(Object.assign({}, props.categories[0])).map((item, index) => {
                  return (
                    <li className="dropdown-li" key={index}>
                      <Link className="dropdown-link2" to={`/categories/${item.category_slug}`}>
                        {item.category_name}
                      </Link>
                      {Object.values(Object.assign({}, props.categories[item.uid])).map((sitem, sindex) => {
                      return (
                      <ul>
                        <li className="dropdown-li" style={{marginLeft:'15px'}}>
                          <Link className="dropdown-link2" to={`/categories/${sitem.category_slug}`}>
                          {sitem.category_name}
                          </Link>
                          {Object.values(Object.assign({}, props.categories[sitem.uid])).map((titem, sindex) => {
                          return (
                          <ul>
                            <li className="dropdown-li" style={{marginLeft:'15px'}}>
                              <Link className="dropdown-link2" to={`/categories/${titem.category_slug}`}>
                              {titem.category_name}
                              </Link>
                            </li>
                          </ul>
                          )
                          })}
                        </li>
                      </ul>
                      )
                      })}
                    </li>
                  )
                })
              }
            </ul> */}
          </li>
        </ul>
      </li>
      <li className="has-dropdown">
        <Link className="menu-item" to="#">
          Brands <i className="rt-plus" />
        </Link>
        <ul className="dropdown-ul">
          {
            props.brand.map(({ brand_name, brand_slug }, index) => {
              return (
                <li className="dropdown-li" key={index}>
                  <Link className="dropdown-link" to={`/brands/${brand_slug}`}>
                    {brand_name}
                  </Link>
                </li>
              )
            })
          }
        </ul>
      </li>
      <li>
        <Link className="menu-item" to={`/products`}>
          Prodcuts
        </Link>
      </li>
      <li>
        <Link className="menu-item" to={`/blogs`}>
          Blog
        </Link>
      </li>
      <li>
        <Link className="menu-item" to={`/contact`}>
          Contact
        </Link>
      </li>
    </React.Fragment>
  )
}
