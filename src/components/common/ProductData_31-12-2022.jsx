import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import SocialList from "../General/SocialList";
import { Link } from "react-router-dom";
import { GetPrice } from "../Helpers";
import { useTranslation } from 'react-i18next';
export default function ProductData_31_12_2022(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    product_images,
    product_image,
    product_name,
    product_price,
    product_ref_price,
    Brand,
    Category,
    product_sku,
    slug,
    product_description,
    product_uid,
    product_variations,
    product_quantity,
    product_stock_out
  } = props.data;
  const [quantity, setQuantity] = useState(1);
  const [vcolor, setVcolor] = useState({ title: "Black" });
  const [vsize, setVsize] = useState({ title: "XXL" });
  var fno = "";
  var sno = "";
  var extraClass = "";
  return (
    <React.Fragment>
      <div className="details-product-area mb--70">
        <div className="product-thumb-area">
          <div className="cursor">
            {Object.values(Object.assign({}, product_images)).map(
              (img, index) => {
                if (index == 0) {
                  fno = "one";
                  extraClass = "figure";
                } else if (index == 1) {
                  fno = "two";
                  extraClass = "hide";
                } else if (index == 2) {
                  fno = "three";
                  extraClass = "hide";
                }
                return (
                  <div
                    className={`thumb-wrapper ${fno} filterd-items ${extraClass}`}
                  >
                    <div
                      className="product-thumb zoom"
                      onmousemove="zoom(event)"
                      style={{
                        backgroundImage: "url('" + img + "')",
                      }}
                    >
                      <img src={img} alt="product-thumb" />
                    </div>
                  </div>
                );
              }
            )}
          </div>
          <div className="product-thumb-filter-group">
            {Object.values(Object.assign({}, product_images)).map(
              (img, index) => {
                if (index == 0) {
                  sno = "one";
                } else if (index == 1) {
                  sno = "two";
                } else if (index == 2) {
                  sno = "three";
                }
                return (
                  <div
                    className="thumb-filter filter-btn active"
                    data-show={`.${sno}`}
                    key={index}
                  >
                    <img src={img} alt="product-thumb-filter" />
                  </div>
                );
              }
            )}
          </div>
        </div>
        <div className="contents">
          <div className="product-status">
            <span className="product-catagory">{Category}</span>
            <div className="rating-stars-group">
              <div className="rating-star">
                <i className="fas fa-star" />
              </div>
              <div className="rating-star">
                <i className="fas fa-star" />
              </div>
              <div className="rating-star">
                <i className="fas fa-star-half-alt" />
              </div>
              <span>{t('product_details.reviews')}</span>
            </div>
          </div>
          <h2 className="product-title">
            {product_name}
            {(parseInt(product_quantity) == parseInt(product_stock_out)) ?
              <span className="stock">{t('product_details.out_of_stock')}</span>
              : <span className="stock">{t('product_details.in_stock')}</span>}
          </h2>
          <span className="product-price">
            <span className="old-price">{GetPrice(product_price)}</span>  {GetPrice(product_ref_price)}
          </span>
          <p>{product_description}</p>
          {/* {Object.values(Object.assign({}, product_variations)).length > 0
            ? Object.values(Object.assign({}, product_variations)).map(
              (data, index) => {
                return (
                  <div className={`action-item3`}>
                    <div className="action-top">
                      <span className="action-title">
                        {data.title} :{" "}
                        <strong>
                          {data.title === "Size" ? vsize.title : vcolor.title}
                        </strong>
                      </span>
                    </div>
                    {Object.values(
                      Object.assign({}, data.variation_option)
                    ).map((vo) => {
                      return (
                        <div
                          className="color-item2"
                          onClick={() =>
                            data.title === "Size"
                              ? setVsize({
                                variation_uid: data.uid,
                                variation_options_uid: vo.uid,
                                title: vo.title,
                              })
                              : setVcolor({
                                variation_uid: data.uid,
                                variation_options_uid: vo.uid,
                                title: vo.title,
                              })
                          }
                        >
                          <div
                            className={`size ${vo.title.toLowerCase()}`}
                            data-bs-toggle="tooltip"
                            data-bs-placement="top"
                            title=""
                            data-bs-original-title={vo.title}
                            aria-label={vo.title}
                          >
                            {vo.title}
                          </div>
                        </div>
                      );
                    })}
                  </div>
                );
              }
            )
            : ""} */}
          <div className="product-bottom-action">
            <div className="cart-edit">
              <div className="quantity-edit action-item">
                <button
                  className="button"
                  onClick={() =>
                    quantity > 1 ? setQuantity(quantity - 1) : setQuantity(1)
                  }
                >
                  <i className="fal fa-minus minus" />
                </button>
                <input type="text" className="input" value={quantity} />
                <button
                  className="button plus"
                  onClick={() => setQuantity(quantity + 1)}
                >
                  +<i className="fal fa-plus plus" />
                </button>
              </div>
            </div>
            <Link
              className="addto-cart-btn action-item"
              disabled={(parseInt(product_quantity) === parseInt(product_stock_out)) ? "disabled" : ""}
              onClick={() =>
                dispatch({
                  type: "ADD",
                  payload: {
                    id: product_uid,
                    slug: slug,
                    title: product_name,
                    image: product_image,
                    quantity: quantity,
                    price: product_price,
                    brand: Brand,
                    category: Category,
                  },
                })
              }
            >
              <i className="rt-basket-shopping" /> {t('product_details.add_to_cart')}
            </Link>
            {
              (parseInt(product_quantity) !== parseInt(product_stock_out))
                ?
                <Link
                  className="wishlist-btn action-item"
                  onClick={() =>
                    dispatch({
                      type: "WISHLIST_ADD",
                      payload: {
                        id: product_uid,
                        slug: slug,
                        title: product_name,
                        image: product_image,
                        quantity: quantity,
                        price: product_price,
                        brand: Brand,
                        category: Category,
                        quantity: product_quantity,
                        stock_out: product_stock_out
                      },
                    })
                  }
                >
                  <i className="rt-heart" />
                </Link>
                : ""
            }

          </div>
          <div className="product-uniques">
            <span className="sku product-unipue">
              <span>{t('product_details.sku')} </span> {product_sku}
            </span>
            <span className="catagorys product-unipue">
              <span>{t('product_details.categories')} </span> {Category}
            </span>
            <span className="tags product-unipue">
              <span>{t('product_details.brand')} </span> {Brand}
            </span>
          </div>
          <div className="d-flex align-items-center share-social">
            <span>{t('product_details.share')}</span>
            <SocialList />
          </div>
        </div>
      </div>
    </React.Fragment >
  );
}
