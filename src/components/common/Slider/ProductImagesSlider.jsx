import React, { useState, useEffect } from "react";
import "./product-image-slider.css";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Thumbs } from "swiper";
import { Link } from "react-router-dom";

import { Variation } from "../../Products/Variations";

export default function ProductImagesSlider(props) {
  const [activeThumb, setActiveThumb] = useState();
  const [images, setImages] = useState([]);

  useEffect(() => {
    if (!images.includes(props.image)) {
      setImages([...images, props.image]);
    }
    if (
      Object.values(props.selectVariation).length > 0 &&
      !images.includes(props.selectVariation.product_image)
    ) {
      setImages([...images, props.selectVariation.product_image]);
    }
  }, [props]);

  return (
    <React.Fragment>
      <Swiper
        spaceBetween={10}
        modules={[Navigation, Thumbs]}
        thumbs={{ swiper: activeThumb }}
        className="product-images-slider"
      >
        {images.map((item, index) => (
          <SwiperSlide key={index}>
            <Link to={`/product/${props.slug}`}>
              <img src={item} alt="product images" />
            </Link>
          </SwiperSlide>
        ))}
      </Swiper>

      <Variation
        type={"image"}
        setActiveThumb={setActiveThumb}
        images={images}
        slug={props.slug}
      />
      <Variation type={"size"} slug={props.slug} />
      <Variation type={"color"} slug={props.slug} />
    </React.Fragment>
  );
}
