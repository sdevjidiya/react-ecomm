//** general */
export { default as BackToPage } from "./General/BackToPage";
export { default as Preload } from "./General/Preload";
export { default as Toast } from "./General/Toast";
export { default as NotFound } from "./General/NotFound";

//* general/header */
export { default as CartIcon } from "./General/Header/CartIcon";
export { default as SearchBoxIcon } from "./General/Header/SearchBoxIcon";
export { default as UserIcon } from "./General/Header/UserIcon";
export { default as WishlistIcon } from "./General/Header/WishlistIcon";
export { default as SearchBox } from "./General/Header/SearchBox";

//** common */
export { default as Header } from "./common/Header";
export { default as TopHeader } from "./common/Header/TopHeader";
export { default as TopHeaderMenu } from "./common/Header/TopHeaderMenu";
export { default as MainMenu } from "./common/Header/MainMenu";
export { default as Menu } from "./common/Header/Menu";
export { default as SideBar } from "./common/Header/SideBar";
export { default as MobileMenu } from "./common/Header/MobileMenu";
export { default as HeaderCartItem } from "./common/Header/HeaderCartItem";
export { default as Cart } from "./common/Header/Cart";

export { default as Footer } from "./common/Footer";
export { default as Preloader } from "./common/Preloader";
export { default as ProductDetailsPopup } from "./common/ProductDetailsPopup";
export { default as Meta } from "./common/Meta";

export { default as BreadCrumb } from "./common/BreadCrumb";
export { default as FeaturedItemsList } from "./common/FeaturedItemsList";
export { default as ProductData } from "./common/ProductData";
export { default as Pagination } from "./common/Pagination";

export { default as NotFoundPage } from "./common/NotFound";

//** home */
export { default as Banner } from "./Home/Banner";
export { default as Offer } from "./Home/Offer";
export { default as CategoryItemsList } from "./Home/CategoryItemsList";
export { default as OffersPoster } from "./Home/OffersPoster";
export { default as Deals } from "./Home/Deals";
export { default as HomeBrands } from "./Home/HomeBrands";

//** account */
export { default as UserOverview } from "./Account/UserOverview";
export { default as PasswordChange } from "./Account/PasswordChange";
export { default as BillingAddress } from "./Account/BillingAddress";
export { default as UpdateProfile } from "./Account/UpdateProfile";

//** products */
export { default as Filter } from "./Products/Filter";
export { default as ItemContent } from "./Products/ItemContent";
export { default as FilterCount } from "./Products/FilterCount";
export { default as ItemCard } from "./Products/ItemCard";
export { default as Coupon } from "./Products/Coupon";
export { default as CouponSuccess } from "./Products/CouponSuccess";
export { default as PriceSlider } from "./Products/PriceSlider";

//** orders */
export { default as OrderItems } from "./Orders/OrderItems";
export { default as OrderSearchBox } from "./Orders/OrderSearchBox";

//** checkout */
export { default as AddressSection } from "./Checkout/Address";
export { default as ChangeAddress } from "./Checkout/ChangeAddress";
export { default as NewAddress } from "./Checkout/NewAddress";

//** cart */
export { default as CartProduct } from "./Cart/CartProduct";

//** blogs */
export { default as BlogCard } from "./Blogs/BlogCard";
export { default as BlogDetails } from "./Blogs/BlogDetails";
export { default as Comment } from "./Blogs/Comment";
export { default as Rating } from "./Blogs/Rating";
export { default as Search } from "./Blogs/Search";
export { default as PopularBlogs } from "./Blogs/PopularBlogs";
export { default as Categories } from "./Blogs/Categories";
export { default as Tags } from "./Blogs/Tags";
export { default as PopularBlogsCard } from "./Blogs/PopularBlogsCard";

//** custom popup */
export { default as CustomPopUp } from "./CustomPopUp";

/* Faq */
export { default as FaqCard } from "./Faq/FaqCard";
