import React from 'react'

export default function Rating() {
  return (
    <div className="rating-area">
      <div className="rating-text">
        <h2 className="text">Give Your Opinion</h2>
      </div>
      <div className="rating-icon">
        <span className="one">
          <a href="#">
            {" "}
            <i className="fas fa-star" />
          </a>
        </span>
        <span className="two">
          <a href="#">
            {" "}
            <i className="fas fa-star" />
          </a>
        </span>
        <span className="three">
          <a href="#">
            {" "}
            <i className="fas fa-star" />
          </a>
        </span>
        <span className="four">
          <a href="#">
            {" "}
            <i className="fal fa-star" />
          </a>
        </span>
        <span className="five">
          <a href="#">
            {" "}
            <i className="fal fa-star" />
          </a>
        </span>
      </div>
    </div>
  )
}
