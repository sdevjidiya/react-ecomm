import React from 'react'

function Comment() {
  return (
    <React.Fragment>
      <div className="comment-header">
        <div className="comment">
          <h3>04 Comment</h3>
        </div>
        <div className="icon">
          <i className="fal fa-comments" />
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <div className="comment-section">
            <div className="comment-text">
              <div className="commentator">
                <a href="#">
                  <img
                    src={process.env.REACT_APP_URL + "/assets/images/blog-details/commentator-1.jpg"}
                    alt="commentator"
                  />
                </a>
              </div>
              <div className="text">
                <div className="section-title">
                  <div className="title">
                    <h2 className="sub-title">Rosalina Kelian</h2>
                    <span className="sect-title">
                      <a href="#">
                        <i className="fal fa-calendar-alt" />
                        24th March 2022
                      </a>
                    </span>
                  </div>
                  <div className="button">
                    <a href="#">
                      <i className="fal fa-reply" /> Reply
                    </a>
                  </div>
                </div>
                <p className="description">
                  But that's not all. Not to be outdone, individual
                  sellers have increasingly engaged in e-commerce
                  transactions via their own personal websites. And
                  digital marketplaces such as eBay or Etsy serve as
                  exchanges where multitudes of buyers and sellers come
                  together to conduct business. commerce has changed the
                  way people shop and consume products and services.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-2" />
        <div className="col-lg-10">
          <div className="comment-section">
            <div className="comment-text">
              <div className="commentator">
                <a href="#">
                  <img
                    src={process.env.REACT_APP_URL + "/assets/images/blog-details/commentator-2.jpg"}
                    alt="commentator"
                  />
                </a>
              </div>
              <div className="text">
                <div className="section-title">
                  <div className="title">
                    <h2 className="sub-title">Alonso William</h2>
                    <span className="sect-title">
                      <a href="#">
                        <i className="fal fa-calendar-alt" />
                        24th March 2022
                      </a>
                    </span>
                  </div>
                  <div className="button">
                    <a href="#">
                      <i className="fal fa-reply" /> Reply
                    </a>
                  </div>
                </div>
                <p className="description">
                  Ecommerce has changed the way people shop and consume
                  products and services. More and more people are turning
                  to their computers and smart devices to order goods,
                  which can easily be delivered to their homes. As such,
                  it has disrupted the retail landscape. Amazon and
                  Alibaba have gained considerable popularity.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-2" />
        <div className="col-lg-10">
          <div className="comment-section">
            <div className="comment-text">
              <div className="commentator">
                <a href="#">
                  <img
                    src={process.env.REACT_APP_URL + "/assets/images/blog-details/commentator-3.jpg"}
                    alt="commentator"
                  />
                </a>
              </div>
              <div className="text">
                <div className="section-title">
                  <div className="title">
                    <h2 className="sub-title">Miranda Halim</h2>
                    <span className="sect-title">
                      <a href="#">
                        <i className="fal fa-calendar-alt" />
                        24th March 2022
                      </a>
                    </span>
                  </div>
                  <div className="button">
                    <a href="#">
                      <i className="fal fa-reply" /> Reply
                    </a>
                  </div>
                </div>
                <p className="description">
                  commerce has changed the way people shop and consume
                  products and services. More and more people are turning
                  to their computers and smart devices to order goods,
                  which can easily be delivered to their homes. As such,
                  it has disrupted the retail landscape. Amazon and
                  Alibaba have gained considerable popularity
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Comment