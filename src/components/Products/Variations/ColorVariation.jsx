import React, { useState, useEffect, useContext } from "react";
// import { MyContext } from '../../common/Item';

export default function ColorVariation({
  variation_option2_name,
  variation_2,
}) {
  const [colorVariations, setColorVariations] = useState({});
  // const { setColorVariation } = useContext(MyContext);

  const clickHandler = (option) => {
    setColorVariations({
      ...colorVariations,
      uid: variation_2,
      option_uid: option.uid,
      name: option.name,
    });
    // setColorVariation({ uid: variation_2, option_uid: option.uid, name: option.name });
  };

  return (
    <div className="action-item3 py-0 my-2">
      {variation_option2_name.map((option, index) => {
        return (
          <div
            className="color-item2"
            key={index}
            onClick={() => clickHandler(option)}
          >
            <div
              className={`size ${
                colorVariations.option_uid === option.uid && "active"
              }`}
              data-bs-toggle="tooltip"
              data-bs-placement="top"
              title=""
              data-bs-original-title={option.name}
            >
              {option.name}
            </div>
          </div>
        );
      })}
    </div>
  );
}
