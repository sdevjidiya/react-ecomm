import React, { useEffect, useState, useContext } from 'react'
// import { MyContext } from '../../common/Item';

export default function SizeVariation({ variation_option1_name, variation_1, context }) {
  const [sizeVariations, setSizeVariations] = useState({});
  // const { setSizeVariation } = useContext(MyContext);

  const clickHandler = (option) => {
    setSizeVariations(
      { ...sizeVariations, uid: variation_1, option_uid: option.uid, name: option.name }
    )
    // context 
    // setSizeVariation({ uid: variation_1, option_uid: option.uid, name: option.name });
  }

  return (
    <div className="action-item3 py-0 my-2">
      {variation_option1_name.map((option, index) => {
        return (
          <div className="color-item2" key={index} onClick={() => clickHandler(option)}>
            <div className={`size ${sizeVariations.option_uid === option.uid && "active"}`} data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title={option.name}>{option.name}</div>
          </div>
        );
      }
      )}
    </div>
  )
}
