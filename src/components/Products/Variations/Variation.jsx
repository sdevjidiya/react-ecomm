import React, { useEffect, useState } from "react";
import { Product } from "../../services";
import { ImageVariation, SizeVariation, ColorVariation } from "./";

export default function Variation({ setActiveThumb, images, type, slug, context }) {
  const [variations, setVariations] = useState({});


  useEffect(() => {
    let isCancelled = false;
    !isCancelled && getProductData(slug);
    return () => {
      isCancelled = true;
    };
  }, []);

  const getProductData = async (slug) => {
    await Product.getProduct(slug).then((res) => {
      res.success === 1 && setVariations(res.arrData);
    });
  };

  if (
    type === "test"
  ) {
    return (
      <p>psfsdfdsfsd</p>
    );
  }

  if (
    Object.values(variations).length &&
    variations.variate_1_name &&
    variations.variate_2_name
  ) {
    if (type === "image") {
      return <ImageVariation setActiveThumb={setActiveThumb} images={images} />;
    }
    if (type === "size" && variations.variate_1_name.toLowerCase() === "size") {
      return (
        <SizeVariation
          variation_option1_name={variations.variation_option1_name}
          variation_1={variations.variation_1}
          context={context}
        />
      );
    }
    if (
      type === "color" &&
      variations.variate_2_name.toLowerCase() === "color"
    ) {
      return (
        <ColorVariation
          variation_option2_name={variations.variation_option2_name}
          variation_2={variations.variation_2}
          context={context}
        />
      );
    }
  }
}
