import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation, Thumbs } from 'swiper'

export default function ImageVariation({ setActiveThumb, images }) {
  return (
    <Swiper
      onSwiper={setActiveThumb}
      spaceBetween={10}
      slidesPerView={4}
      modules={[Navigation, Thumbs]}
      className='product-images-slider-thumbs'
    >
      {
        images.map((item, index) => (
          <SwiperSlide key={index}>
            <div className="product-images-slider-thumbs-wrapper">
              <img src={item} alt="product images" />
            </div>
          </SwiperSlide>
        ))
      }
    </Swiper>
  )
}
