import React from 'react'

export default function CommentForm() {
  return (
    <div className="comment-form mb-10">
      <div className="contact-form">
        <div className="row">
          <div className="col-lg-6 col-sm-12">
            <div className="input-box text-input mb-20">
              <textarea
                name="Message"
                id="validationDefault01"
                cols={30}
                rows={10}
                placeholder="Type Your Comments..."
                required=""
                defaultValue={""}
              />
            </div>
          </div>
          <div className="col-lg-6 col-sm-12">
            <div className="col-lg-12">
              <div className="input-box mb-20">
                <input
                  type="text"
                  id="validationDefault02"
                  placeholder="Type Your Name..."
                  required=""
                />
              </div>
            </div>
            <div className="col-lg-12">
              <div className="input-box mail-input mb-20">
                <input
                  type="text"
                  id="validationDefault03"
                  placeholder="Type Your E-mail..."
                  required=""
                />
              </div>
            </div>
            <div className="col-lg-12">
              <div className="input-box sub-input mb-30">
                <input
                  type="text"
                  id="validationDefault04"
                  placeholder="Type Your Website..."
                  required=""
                />
              </div>
            </div>
            <div className="col-12 mb-15">
              <button className="form-btn form-btn4">
                <i className="fal fa-comment"></i>
                Post Comments
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
