export const AppendScript = (scriptToAppend) => {
  const script = document.createElement("script");
  script.src = scriptToAppend;
  // script.defer = true;
  document.body.appendChild(script);
}