import React from 'react'
import { Link } from 'react-router-dom';

export default function BillingAddress() {
  return (
    <React.Fragment>
      <div className="account-main address filterd-items hide">
        <h2 className="mb--30 c-title-flex"><span>Address Book</span> <a href="#" className="btn c-custom-address-btn">Add Address</a></h2>
        <table className="table">
          <thead>
            <tr className="top-tr">
              <th>Address</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            
            <tr>
              <td>#3522 Interstate<br/>
              75 Business Spur,<br></br>
              Sault Ste.<br></br>
              Marie, MI 49783</td>
                <td className="d-flex">
                  <a href="#" className="btn c-custom-address-btn"><i className="fal fa-edit"></i></a>
                  <a href="#" className="btn c-custom-address-btn"><i className="fal fa-trash"></i></a>
                </td>
            </tr>
            <tr>
              <td>#3522 Interstate<br/>
              75 Business Spur,<br></br>
              Sault Ste.<br></br>
              Marie, MI 49783</td>
                <td className="d-flex">
                  <a href="#" className="btn c-custom-address-btn"><i className="fal fa-edit"></i></a>
                  <a href="#" className="btn c-custom-address-btn"><i className="fal fa-trash"></i></a>
                </td>
            </tr>
          </tbody>
        </table>
      </div>
    </React.Fragment>
  )
}
