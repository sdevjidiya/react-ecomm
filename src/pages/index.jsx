export { default as Home } from './Home';
export { default as Contact } from './Contact';
export { default as ProductDetails } from './ProductDetails';
export { default as Cart } from './Cart';
export { default as Products } from './Products';
export { default as CategoryProducts } from './CategoryProducts';
export { default as BrandProducts } from './BrandProducts';
export { default as About } from './About';
export { default as PaymentSuccess } from '../stripe/PaymentSuccess';
export { default as PaymentCancel } from '../stripe/PaymentCancel';

export { default as Checkout } from './Checkout';

export { default as Login } from './Login';
export { default as Account } from './Account';
export { default as Wishlist } from './Wishlist';

export { default as OrderView } from './OrderView';
export { default as OrderItemReturn } from './OrderItemReturn';
export { default as OrderHistory } from './OrderHistory';
export { default as OrderCancel } from './OrderCancel';

export { default as ForgotPassword } from './ForgotPassword';
export { default as ForgotPasswordVerify } from './ForgotPasswordVerify';

export { default as Blogs } from './Blogs';
export { default as Faq } from './Faq';
export { default as PrivacyPolicy } from './PrivacyPolicy';
export { default as TermsOfUse } from './TermsOfUse';

/* address  */
export { default as ListAddress } from './ListAddress'
export { default as AddAddress } from './AddAddress'


