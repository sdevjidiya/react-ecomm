import React from 'react'

import { BreadCrumb } from '../components'
import { useTranslation } from 'react-i18next';

export default function Service() {
  const { t } = useTranslation();
  return (
    <>
      <BreadCrumb />
      <div>{t('service.title')}</div>
    </>
  )
}
