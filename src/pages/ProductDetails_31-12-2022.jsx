import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { BreadCrumb } from "../components";

import {
  ProductData,
  FeaturedItemsList,
  ProductDetailsPopup,
  Meta,
} from "../components";

//** Api
import api from "../components/ApiConfig";
import { Apis } from "../config";
import { useTranslation } from "react-i18next";
import CompanyConfig from "../components/CompanyConfig";
export default function ProductDetails31_12_2022({ title }) {
  const { t } = useTranslation();
  const { slug } = useParams();
  const { company_token } = CompanyConfig();
  const [model, setModel] = useState({});
  const [products, setProducts] = useState([]);
  const [singleProduct, setSingleProduct] = useState([]);

  let Parameter1 = {
    company_token: company_token,
    keyword: "",
    categories: [],
    brands: [],
    price: {
      min: 0,
      max: 0,
    },
    sorting: {
      by: "price",
      order: "desc",
    },
    pagination: {
      per_page: 10,
      page: 1,
    },
  };

  let Parameter2 = {
    slug: slug,
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        await api.post(Apis.ProductList, Parameter1).then((res) => {
          if (res.data.success == 1) {
            setProducts(res.data.arrData);
          }
        });
        await api.post(Apis.GetProduct, Parameter2).then((res) => {
          if (res.data.success == 1) {
            setSingleProduct(res.data.arrData);
          }
        });
      } catch (error) {
        console.error(error.message);
      }
    };

    fetchData();
  }, [slug]);
  return (
    <React.Fragment>
      <Meta
        title={title + singleProduct.meta_title}
        keyword={title + singleProduct.meta_keyword}
        description={title + singleProduct.meta_description}
      />
      <BreadCrumb title={t("product_details.title")} />
      <div className="rts-product-details-section section-gap">
        <div className="container">
          <ProductData data={singleProduct} />
          <div className="product-full-details-area">
            <div className="details-filter-bar2">
              <button
                className="details-filter filter-btn active"
                data-show=".dls-one"
              >
                {t("product_details.tabs.1.title")}
              </button>
              <button
                className="details-filter filter-btn"
                data-show=".dls-two"
              >
                {t("product_details.tabs.2.title")}
              </button>
              <button
                className="details-filter filter-btn"
                data-show=".dls-three"
              >
                {t("product_details.tabs.3.title")}
              </button>
            </div>
            <div className="full-details dls-one filterd-items">
              <div className="full-details-inner">
                <p className="mb--30">
                  {t("product_details.tabs.1.content_1")}
                </p>
                <p>{t("product_details.tabs.1.content_2")}</p>
              </div>
            </div>
            <div className="full-details dls-two filterd-items hide">
              <div className="full-details-inner">
                <p className="mb--30">
                  {t("product_details.tabs.2.content_1")}
                </p>
                <p>{t("product_details.tabs.2.content_2")}</p>
              </div>
            </div>
            <div className="full-details dls-three filterd-items hide">
              <div className="full-details-inner">
                <p>
                  {t(
                    "product_details.tabs.3.reveiw_form.there_are_no_reveiws_yet"
                  )}
                </p>
                <div className="row">
                  <div className="col-lg-12 col-md-12 mr-10">
                    <div className="reveiw-form">
                      <h2 className="section-title">
                        {t(
                          "product_details.tabs.3.reveiw_form.be_the_first_to_reveiw"
                        )}
                        <strong>
                          {" "}
                          <Link to="product-details.html">
                            {t(
                              "product_details.tabs.3.reveiw_form.wide_cotton_tunic_dress"
                            )}
                          </Link>
                        </strong>
                      </h2>
                      <h4 className="sect-title">
                        {t(
                          "product_details.tabs.3.reveiw_form.your_email_address_will_not_be_published"
                        )}
                      </h4>
                      <div className="reveiw-form-main mb-10">
                        <div className="contact-form">
                          <div className="row">
                            <div className="col-lg-6 col-sm-12">
                              <div className="input-box text-input mb-20">
                                <textarea
                                  name="message"
                                  id="validationDefault01"
                                  cols={30}
                                  rows={10}
                                  placeholder={t(
                                    "product_details.tabs.3.reveiw_form.message.placeholder"
                                  )}
                                  required=""
                                  defaultValue={""}
                                />
                              </div>
                            </div>
                            <div className="col-lg-6 col-sm-12">
                              <div className="col-lg-12">
                                <div className="input-box mb-20">
                                  <input
                                    name="name"
                                    type="text"
                                    id="validationDefault02"
                                    placeholder={t(
                                      "product_details.tabs.3.reveiw_form.name.placeholder"
                                    )}
                                    required=""
                                  />
                                </div>
                              </div>
                              <div className="col-lg-12">
                                <div className="input-box mail-input mb-20">
                                  <input
                                    name="email"
                                    type="text"
                                    id="validationDefault03"
                                    placeholder={t(
                                      "product_details.tabs.3.reveiw_form.email.placeholder"
                                    )}
                                    required=""
                                  />
                                </div>
                              </div>
                              <div className="col-lg-12">
                                <div className="rating">
                                  <p>
                                    {t(
                                      "product_details.tabs.3.reveiw_form.your_rating"
                                    )}
                                  </p>
                                  <div className="rating-icon">
                                    <span className="one">
                                      <Link to="/">
                                        {" "}
                                        <i className="fal fa-star" />
                                      </Link>
                                    </span>
                                    <span className="two">
                                      <Link to="/">
                                        {" "}
                                        <i className="fal fa-star" />
                                      </Link>
                                    </span>
                                    <span className="three">
                                      <Link to="/">
                                        {" "}
                                        <i className="fal fa-star" />
                                      </Link>
                                    </span>
                                    <span className="four">
                                      <Link to="/">
                                        {" "}
                                        <i className="fal fa-star" />
                                      </Link>
                                    </span>
                                    <span className="five">
                                      <Link to="/">
                                        {" "}
                                        <i className="fal fa-star" />
                                      </Link>
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div className="col-12 mb-15">
                                <button className="form-btn form-btn4">
                                  {t(
                                    "product_details.tabs.3.reveiw_form.button_submit"
                                  )}
                                  <i className="fal fa-long-arrow-right" />
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <FeaturedItemsList
        icon={"wvbo-icon.png"}
        title={t("product_details.featured_product.title")}
        sub_title={t("product_details.featured_product.sub_title")}
        Objectdata={products}
        setModel={setModel}
      />
      <ProductDetailsPopup data={model} />
    </React.Fragment>
  );
}
